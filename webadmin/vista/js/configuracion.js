;(function($) {
	"use strict"
	$(window).on("load", function() {
		$(".pre-loader").fadeIn("slow")
	})
})(jQuery)

var DIRECCION_WS = "http://localhost:8080/webservice/"

menu()

jQuery(document).ready(function($) {
	if (Cookies.get("token") == null) {
		location.href = "./login.html"
	} else {
		document.getElementById("hud_nombre_usuario").textContent = Cookies.get(
			"nombre_usuario"
		)

		var menus = ["3", "8"]
		var acceso = Cookies.get("acceso")
		var header = ""

		for (var i = 0; i < menus.length; i++) {
			for (var j = 0; j < acceso.length; j++) {
				if (menus[i] == acceso[j]) {
					header = "menu" + acceso[j]
					header2 = "#m_menu" + acceso[j]
					document.getElementById(header).children[0].style.display = "block"
					$(header2).show()
				}
			}
		}
	}
})

$("#btnSalir").on("click", function() {
	Cookies.remove("nombre_usuario")
	Cookies.remove("token")
	Cookies.remove("acceso")
	Cookies.remove("id_usuario_area")

	location.href = "./login.html"
})

function menu() {
	document.getElementById("menu3").children[0].style.display = "none"
	document.getElementById("menu8").children[0].style.display = "none"

	$("#m_menu3").hide()
	$("#m_menu8").hide()
}
