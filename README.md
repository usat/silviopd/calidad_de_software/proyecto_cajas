# SISTEMA WEB DE GESTIÓN DE ALMACÉN

&nbsp;

### INTEGRANTES

- Chambergo Chapilliquén, Sergio
- Felices Dávila, Alexandra
- Peña Diaz, Silvio Reynaldo
- Puelles soplapuco, Jhonatan

### NOMBRE DEL SISTEMA

- SISTEMA WEB DE GESTIÓN DE ALMACÉN

### DELIMITACIÓN DEL SISTEMA

- El sistema esta basado en el control de gestion de las cajas en almacen

### REQUERIMIENTOS FUNCIONALES

- El sistema contará con un inicio de sesión, previamente registrado en caso contrario se tendrá que hablar con el administrador para crear las credenciales.
- El sistema contará con la posibilidad de hacer seguimiento de la ubicacion de las cajas registradas.
- Se permitirá el registro y edicion de cliente, asi como la posibilidad de darle de baja (Deshabilitado)
- El sistema contará con el registro y edicion del nombre de la caja, precio base y precio retraso
- El sistema podrá retirar las cajas multiplicando el precio base por la cantidad de cajas a retirar, luego se mostrará un ticker que se debe imprimir luego.
- El sistema contará con el registro de transacciones que permitirá la suma o resta de cajas al cliente, una vez llegada a 0 las cajas (stock actual) en almacén no se listará y se deberá crear un nuevo registro del número de cajas en almacen a dicho cliente
- El sistema cuenta con un reporte de transacciones totales de las cajas que no pertenencen a la empresa ecomphisa (Reporte De Cajas Registro)
- El sistema cuenta con un reporte de transacciones totales de las cajas que pertenencen a la empresa ecomphisa (Reporte De Cajas Alquiler)
- El sistema contarará con un reporte para las transacciones de cajas nuevas (Reporte De cajas Nuevas)
- El sistema contarará con un reporte para las transacciones de cajones (Reporte De Cajones)
- El sistema contarará con un reporte para las transacciones de sillas (Reporte De Sillas)
- El sistema contarará con un reporte para las transacciones de conteo (Reporte De Conteo)

### REQUERIMIENTOS NO FUNCIONALES

#### De Usabilidad

- El sistema contará con manuales instructivos del uso del sistema
- El sistema cuenta con un diseño WebResponsive para que el cliente pueda realizar sus consultas por el móvil celular.
- El sistema cuenta con una interfaz amigable para el usuario.
- El sistema muestra los errores informativos amigables para el usuario final.
- El tiempo de atención para las ventas se reduce.

#### De Eficiencia

- Toda transacción realizada por el usuario debe responder rápida.
- El sistema debe ser capaz de tolerar multiples realizando transacciones.
- Los datos ingresados al sistema deben ser actualizados.

#### De Seguridad

- Para el ingreso al sistema se tendrá en cuenta tener credenciales de acceso para cada usuario
- El manejo de las transacciones se estará monitoreando en la base de datos, ya que se registrará el usuario que la realizo.
- Si se identifica un fallo o brecha del sistema, el usuario no seguirá operando a menos que el administrador del sistema lo autorice, luego de haber realizado la corrección.

#### El sistema cuenta con 3 partes:

- Back-end: Web Services hecha en el lenguaje php
- Front-end: hecha en html,css3 y javascript
- DataBase: hecha en mysql

#### Requerimientos Técnicos 
- Ancho de banda mínimo recomendable: 8mb de bajada y subida.
- Servidor compatible Apache
- Capacidad de Disco duro mínimo recomendable: 50 GB


### Acceso

- <strong>Usuario:</strong> 46705014
- <strong>Password:</strong> CajaMarca2018
